def get_name_node
  {
    "name" => {
      "attrs" => {"value" => "This is really a LONG Name"},
      "nodes" => {"surn" => "Sur name", "givn" => "Given Name"}
    }
  }
end

def get_custom_tag_name
  {
    "name" => {
      "attrs"=> {"tag_name" => "node"},
      "nodes" => {"surn" => "Sur name"}
    }
  }
end

def get_nested_hash
  {
    "indi" => {
    "nodes" => {
      "name" => {
        "attrs" => {"value" => "This is really a LONG Name"},
        "nodes" => {"surn" => "Sur name", "givn" => "Given Name"}
      }},
    "attrs" => {"id" =>  1234}
  }}
end


def get_custom_attribute_name
  {
    "name" => {
      "attrs"=> {"attr" => 123},
      "nodes" => {"surn" => "Sur name"}
    }}
end

def get_nil_nodes
  {
    "name" => {
      "attrs"=> {"attr" => 123},
      "nodes" => {"surn" => "Sur name", "givn" => nil}
    }}
end