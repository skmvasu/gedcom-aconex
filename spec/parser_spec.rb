require './lib/gedcom/aconex'

describe Parser do
  context "parse data-name.txt" do
    before do
      @file = File.open('fixtures/data-name.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
    end

    after do
      @file.close()
    end

    it "parse name and surname properly" do
      data = @hash['I0001']
      expect(data['attrs']).to eql({"id"=> 'I0001', "tag_name"=> "indi"})
      expect(data['nodes']['name']['attrs']).to eql({"value"=> "Elizabeth Alexandra Mary /Windsor/"})
      expect(data['nodes']['name']['nodes']).to eql({"givn"=> "Elizabeth Alexandra Mary", "surn"=> "Windsor"})
    end

    it "should strip @ while parsing family reference" do
      data = @hash['I0001']
      expect(data['nodes']['famc']).to eql('F0003')
    end

    it "should create nodes at level 1" do
      data = @hash['I0001']
      expect(data['nodes']['sex']).to eql('F')
      expect(data['nodes']['occu']).to eql('Queen')
    end

    it "should nest level 2 values into the current node" do
      data = @hash['I0001']
      expect(data['nodes']['birth']['nodes']).to eql({"date" => "15 Aug 1947"})
    end

    it "line following level 2 should be in its own node" do
      data = @hash['I0001']
      expect(data['nodes']['deat']).to eql('Y')
    end
  end

  context "parse footnotes" do
    before do
      @file = File.open('fixtures/notes.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
    end

    after do
      @file.close()
    end

    it "should parse all footnotes" do
      keys = @hash.keys
      id = "N0040"
      data = @hash[id]
      expect(keys.length).to eql(10)
      expect(keys).to include(id)
      expect(data['attrs']).to eql({"tag_name" => "note", "id"=>id})
    end
  end

  context "parse family data" do
    before do
      @file = File.open('fixtures/family.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
      @id = "F0023"
      @data = @hash[@id]
    end

    after do
      @file.close()
    end

    it "should parse family info" do
      expect(@data['attrs']).to eql({"tag_name"=>"fam", "id" => @id})
      expect(@data['nodes']['husb']).to eql('I0063')
    end

    it "should nest level 2 values into the current node" do
      expect(@data['nodes']['marr']["nodes"]).to eql({"date" => "1889"})
    end
  end

  context "parse empty file" do
    before do
      @file = File.open('fixtures/empty.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
    end

    after do
      @file.close()
    end

    it "should run through empty files without throwing up" do
      expect(@hash).to eql({})
    end

  end

  context "handle invalid EOF" do
    before do
      @file = File.open('fixtures/EOF.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
    end

    after do
      @file.close()
    end

    it "should run through files with TRLF without throwing up" do
      data = @hash['I0001']
      expect(@hash.keys.length).to eql(1)
      expect(data['attrs']).to eql({"id"=> 'I0001', "tag_name"=> "indi"})
      expect(data['nodes']['name']['attrs']).to eql({"value"=> "Elizabeth Alexandra Mary /Windsor/"})
      expect(data['nodes']['name']['nodes']).to eql({"givn"=> "Elizabeth Alexandra Mary", "surn"=> "Windsor"})
    end
  end

  context "handle n level data" do
    before do
      @file = File.open('fixtures/multilevel.txt', 'r')
      parsed = Parser.parse_content(@file)
      @hash = parsed["gedcom"]["nodes"]
    end

    after do
      @file.close()
    end

    it "should parse level 3 data" do
      data = @hash['I0001']
      expect(@hash.keys.length).to eql(2)

    end
  end



end
