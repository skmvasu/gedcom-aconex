require './lib/gedcom/hash'
require './fixtures/gedcom_factory'

describe 'Convert hashes to gedcom XML' do

  it 'should should pass value as a param to parent node' do
    hash = get_name_node()
    xml = hash.to_gedcom_xml
    expect(xml).to eql("<name value='This is really a LONG Name'><surn>Sur name</surn><givn>Given Name</givn></name>")
  end

  it 'should should override tag_name when passed' do
    hash = get_custom_tag_name()
    xml = hash.to_gedcom_xml
    expect(xml).to eql("<node><surn>Sur name</surn></node>")
  end

  it 'should should convert nested values' do
    hash = get_nested_hash()
    xml = hash.to_gedcom_xml
    expect(xml).to eql("<indi id='1234'><name value='This is really a LONG Name'><surn>Sur name</surn><givn>Given Name</givn></name></indi>")
  end

  it 'should skip custom attribuets other than value/id' do
    hash = get_custom_attribute_name()
    xml = hash.to_gedcom_xml
    expect(xml).to eql("<name><surn>Sur name</surn></name>")
  end
  it 'should handle nil values' do
    hash = get_nil_nodes()
    xml = hash.to_gedcom_xml
    expect(xml).to eql("<name><surn>Sur name</surn></name>")
  end
end

