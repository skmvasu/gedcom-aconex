class Aconex
  def self.read_file(file)
    Parser.parse_content(file)
  end
end

class Parser
  def self.parse_content(file)
    parsed_data = {'gedcom' => {'nodes' => {}}}
    root = parsed_data['gedcom']['nodes']
    prev_node = {}
    prev_level = 0
    id = nil

    file.each do |line|
      line = line.strip
      if line.nil? or line.empty?
        next
      end

      level, tag, trailer = line.split(' ', 3)

      if level.to_i == 0
        if trailer.nil? or tag.nil?
          next
        end

        id = self.parse_id(tag)

         root[id] = {
            'attrs'=> {'id'=> id, 'tag_name'=> trailer.strip.downcase},
            'nodes'=> {},
            'parent' => root
         }

        prev_node = root[id]

      else
        node_to_attach = prev_node.empty? ? root[id] : getNodeToAttach(prev_node, prev_level.to_i, level.to_i)
        if node_to_attach.nil? or node_to_attach.empty?
          node_to_attach
        end
        if (trailer.nil? || trailer.empty?)
          node_to_attach['nodes'][tag.downcase] = {
              'nodes'=> {},
              'parent' => node_to_attach
          }

          prev_node = node_to_attach['nodes'][tag.downcase]
        else
          value = tag == 'NAME' ? parse_name(trailer) : self.parse_id(trailer)
          if (node_to_attach['nodes'].nil?)
            node_to_attach['nodes']
          end
          node_to_attach['nodes'][tag.downcase] = value
          prev_node = node_to_attach
        end
        prev_level = level.to_i
      end
    end
    parsed_data
  end


  def self.parse_id(id)
    id.strip.gsub('@', '')
  end

  def self.parse_name(name)
    # strip off trailing \n
    name = name.strip
    givn, surn = name.split('/', 3)
    {
        'attrs'=> {'value'=> name},
        'nodes'=> {
            'givn'=> givn.strip,
            'surn'=> surn ? surn.strip : nil
        }
    }
  end
end

def getNodeToAttach(node, level,  current_level)
  if (level > current_level)
    getNodeToAttach(node['parent'], level-1, current_level)
  else
    node
  end
end
