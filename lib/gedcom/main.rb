require_relative './aconex'
require_relative './hash'
require 'fileutils'


def parse_gedcom(file)
  puts "Parsing #{file.filename}"
  parsed_data = Aconex.read_file(file)
  puts "Generating xml tree"
  xml = parsed_data.to_gedcom_xml()

  path = 'output'

  puts "writing to #{path}/gedcom.xml"
  unless File.directory?(path)
    puts "Directory #{path} not found. Creating it."
    FileUtils.mkdir_p(path)
  end

  File.open("#{path}/gedcom.xml", 'w') do |f|
    f.write(xml)
    f.close()
  end

  puts "Done and dusted!!!"
end

if ARGV.empty?
  puts "Usage ruby lib/gedcom/main.rb data/sample.txt"
  exit
end

parse_gedcom(ARGF)