# Overriding Ruby's hash to support xml conversion

class Hash
  def to_gedcom_xml
   map do |k, v|
      unless v.nil?
      attrs = v["attrs"]
      value = Hash === v["nodes"] ? v["nodes"].to_gedcom_xml : v

      tag_name, attr_value, attr_id = attrs.values_at("tag_name", "value", "id") unless attrs.nil?
      tag_name = k if tag_name.nil?

      if attr_value.nil? and attr_id.nil?
        "<#{tag_name}>#{value}</#{tag_name}>"
      else
        attr_key = attr_value.nil? ? "id" : "value"

        "<#{tag_name} #{attr_key}='#{attr_value or attr_id}'>#{value}</#{tag_name}>"
      end
      end
    end.join
  end
end