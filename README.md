##### Getting started

- bundle install to install all dependencies (just rspec for tests)
- To run `ruby lib/gedcom/main.rb ./data/sample.txt`
- The result will be saved in `output/gedcom.xml`

##### Design

Gedcom parser is an interesting problem, more so because I didn't know such a format existed and it was a lot of fun to work on :)

The tech approach to this problem is simple. 

Any XML node can have only 2 properties. 

- Attributes
- other nodes/ String

In the first pass we're building hash to represent this. It roughly looks like 
	```
	key: {
		attributes: {id: 123},
		nodes: {name: "Vasu"}
	} 
	```
The nodes here can be a simple string/ another node. For instance the lines
```
0 @I111@ INDI

1 name "Vasu"
```

will become
``` 
indi: {
	attrs: {value: I111},
	nodes: {name: Vasu}
}
```

Here name is a special node and needs some love. We're splitting the name into Given name and Surname based on the `/` delimitters.

Once all the lines in the file are parsed, now we've to generate the XML document. We're overriding Hash to parse this hash and generate an xml string.

This recursively parses all nodes in this hash and spits out an XML string. We're then writing this string into an XML file.

###### Boundary cases

- The script expects a valid gedcom file path to be passed. If you don't pass a data file, it errors out. 
- Output filepath is not customizable right now. It overwrites the file `output/gedcom.xml` everytime the script runs.
- The generated XML is NOT formatted. 
